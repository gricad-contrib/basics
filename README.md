# Quelques tests basiques pour Tensofflow/Keras

Voici quelques notebooks et scripts pour tester votre environnement Tensorflow/Keras  
Avec GPU ou sans...

## Tests disponibles ?

Les tests disponibles sont les suivants :

**Krypton :**
  - CNN de classification
  - Dataset de 70.000 images, 64x64 pixels, monochromes.
  - 2.2 G, données fournies, dans ./data
  - Environ 15' sur CPU, 45'' sur GPU

## Comment les utiliser ?

Depuis un environnement offrant Tensorflow/Keras  

2 solutions :
  - Notebook : ouvrir le notebook (jupyter lab)
  - Script : lancer le script

**Exemple :**  
```
./Krypton.py

==================================================
Deeptests - Krypton example (Tensorflow/Keras/CNN)
==================================================

Now is             :  Monday 9 December 2019, 17:30:17
TensorFlow version :  1.14.0
Keras version      :  2.2.4-tf
Hide warning       :  True
Kypton version     :  0.1
Dataset size       :  10.0%

(...)

Summary
-------

Duration/load   :  6.822 sec.
Duration/model  :  0.173 sec.
Duration/run    : 101.063 sec.
Duration/test   :  6.964 sec.

Duration/total  : 115.022 sec.

That's all folks :-)
```

**Note :**  
Pour générer les scripts python correspondants aux notebooks, voir le notebook : **Notebooks to scripts.ipynb**



