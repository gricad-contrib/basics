#!/usr/bin/env python
# coding: utf-8

# Krypton Example - CNN
# ===========================================
# Deep Neural Network (DNN) with Tensor Flow (Keras)  
# JL Parouty - December 2019
# 
# Few parameters :
#   - **verbose_fit** : verbose level during train
#     - 0 : silent
#     - 1 : progress bar
#     - 2 : one line/epoch
#   - **hide_warning** : hide warning (True) or not (False)
#   - **dataset_size** : % of dataset we are going to use - Use 0.1 for small tests.

# In[ ]:


import tensorflow as tf
from tensorflow import keras

import numpy as np
import time
import datetime
import logging

VERSION      = '0.1'
data_dir     = './data/Krypton64/'

hide_warning = True
show_summary = False
verbose_fit  = 2
dataset_size = 1


# In[ ]:


# ---- Here and now
now = datetime.datetime.now()

# ---- Warnings
# logging.getLogger('tensorflow').disabled = hide_warning
tf.autograph.set_verbosity(0)

print('\n==================================================')
print('Deeptests - Krypton example (Tensorflow/Keras/CNN)')
print('==================================================\n')
print('Now is             :  {:s}'.format(now.strftime("%A %-d %B %Y, %H:%M:%S")))
print('TensorFlow version : ',tf.__version__)
print('Keras version      : ',tf.keras.__version__)
print('Hide warning       : ',hide_warning)
print('Kypton version     : ',VERSION)
print('Dataset size       :  {:3.1f}%'.format(100*dataset_size))


# ## Load data

# In[ ]:


t0 = datetime.datetime.now()

img_rows, img_cols = 64,64
input_shape = (img_rows, img_cols, 1)
train_size = int(60000*dataset_size)

# ---- Get data
dataset_x=np.load(data_dir+'dataset-x.npy')
dataset_y=np.load(data_dir+'dataset-y.npy')

# ---- Shuffle it
p = np.random.permutation(len(dataset_x))
dataset_x = dataset_x[p]
dataset_y = dataset_y[p]

# ---- Reshape and normalize
dataset_x = dataset_x.reshape( dataset_x.shape[0], img_rows, img_cols, 1)
dataset_x = dataset_x.astype('float32') / 255

# ---- train/test sets
x_train = dataset_x[:train_size]
y_train = dataset_y[:train_size]

x_test = dataset_x[60000:]
y_test = dataset_y[60000:]

t1  = datetime.datetime.now()
dt1 = t1-t0

print("\n1. Load data")
print("   ---------\n")
print("Data directory  : ",data_dir)
print("x_train         : ",x_train.shape)
print("y_train         : ",y_train.shape)
print("x_test          : ",x_test.shape)
print("y_test          : ",y_test.shape)
print("Duration/load   : {:6.3f} sec.".format(dt1.total_seconds()))


# ## Create model

# In[ ]:


t0 = datetime.datetime.now()

batch_size  =  64
num_classes =  3
epochs      =  5

model = keras.models.Sequential()
model.add( keras.layers.Conv2D(48, (3, 3), activation='relu', input_shape=(img_rows, img_cols, 1))) #32
model.add( keras.layers.MaxPooling2D((2, 2)))
model.add( keras.layers.Conv2D(96, (3, 3), activation='relu')) #32
model.add( keras.layers.MaxPooling2D((2, 2)))
model.add( keras.layers.Conv2D(192,(3, 3), activation='relu')) #64
model.add( keras.layers.MaxPooling2D((2, 2)))
model.add( keras.layers.Conv2D(256,(3, 3), activation='relu')) #64
model.add( keras.layers.MaxPooling2D((2, 2)))
model.add( keras.layers.Flatten()) 
model.add( keras.layers.Dense(1024, activation='relu')) # 576
model.add( keras.layers.Dense(200, activation='relu'))
model.add( keras.layers.Dense(50, activation='relu'))
model.add( keras.layers.Dense(3, activation='softmax'))
if show_summary: 
    model.summary()

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

t1  = datetime.datetime.now()
dt2 = t1-t0

print("\n2. Create and compile model")
print('   ------------------------\n')
print("Duration/model  : {:6.3f} sec.".format(dt2.total_seconds()))


# ## Run it...

# In[ ]:


print("\n3. Run model")
print('   ---------\n')
t0 = datetime.datetime.now()

history = model.fit(  x_train, y_train,
                      batch_size=batch_size,
                      epochs=epochs,
                      verbose=verbose_fit,
                      validation_data=(x_test, y_test))

t1  = datetime.datetime.now()
dt3 = t1-t0
print("\nDuration/run    : {:6.3f} sec.".format(dt3.total_seconds()))


# ### Test it

# In[ ]:


print("\n4. Test model")
print('   ----------\n')

t0  = datetime.datetime.now()
score = model.evaluate(x_test, y_test, verbose=2)
t1  = datetime.datetime.now()
dt4 = t1-t0

print('\nTest loss       : {:6.3f}'.format(score[0]))
print('Test accuracy   : {:6.3f}'.format(score[1]))
print("Duration/test   : {:6.3f} sec.".format(dt4.total_seconds()))


# ### Summary

# In[ ]:


print("\nSummary")
print("-------\n")
print("Duration/load   : {:6.3f} sec.".format(dt1.total_seconds()))
print("Duration/model  : {:6.3f} sec.".format(dt2.total_seconds()))
print("Duration/run    : {:6.3f} sec.".format(dt3.total_seconds()))
print("Duration/test   : {:6.3f} sec.".format(dt4.total_seconds()))

dt=dt1+dt2+dt3+dt4
print("\nDuration/total  : {:6.3f} sec.".format(dt.total_seconds()))

print("\nThat's all folks :-)")


# In[ ]:




